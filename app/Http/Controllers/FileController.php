<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class FileController extends Controller
{
    public function index(Request $request){
       $name = session('name');
       $password = session('password');
       if($name == "admin" && $password == 'admin'){
          return view('admin/index');
       }
       return redirect('/login');
    }

    public function viewFileContent(Request $request){
        $readFile = array();
        if(!file_exists($request->file_path)) {
            return response()->json('1');
        }
        $handle = fopen($request->file_path, "r");
        if ($handle) {
            while (($line = fgets($handle)) !== false) {
                // process the line read.
                $readFile[] = $line;
            }
            fclose($handle);
        }
        return response()->json($readFile);
    }
}
