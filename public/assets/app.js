$(document).ready (function () {
    var readFile = [];
    $('.submit').on('click',function(event){
        event.preventDefault();
        var file_path = $('#file_path_id').val();
        var rowsTotalBody = $('#data tbody tr').length;
        if(rowsTotalBody <= 0){
            axios.post("/", {file_path: file_path})
            .then((response) => {

            if(response.data == '1'){
                console.log(response);
               $('#file_path_id').css('border-color', 'red');
               return;
            }
            readFile = response.data;
            $.each(readFile, function(key,val) {
               $('#table_body').append(`
               <tr>
               <td>${key + 1}</td>
               <td>${val}</td>
               <tr>
               `);

            });
            pagination();
           });
        }
    });

   function pagination(){
    $('#data').after ('<div id="nav"></div>');
    var rowsShown = 20;
    var rowsTotal = $('#data tbody tr').length;
    var numPages = rowsTotal/rowsShown;
    $('#nav').append ('<a href="#" rel="'+0+'"><</a> ');
    for (i = 0;i < numPages;i++) {
        var pageNum = i + 1;
        $('#nav').append ('<a href="#" rel="'+i+'">'+pageNum+'</a> ');
    }
    $('#nav').append (`<a href="#" rel="${pageNum-1}">></a>`);
    $('#data tbody tr').hide();
    $('#data tbody tr').slice (0, rowsShown).show();
    $('#nav a:first').addClass('active');
    $('#nav a').bind('click', function() {
    $('#nav a').removeClass('active');
   $(this).addClass('active');
        var currPage = $(this).attr('rel');
        var startItem = currPage * rowsShown;
        var endItem = startItem + rowsShown;
        $('#data tbody tr').css('opacity','0.0').hide().slice(startItem, endItem).
        css('display','table-row').animate({opacity:1}, 300);
    });
   }

});
