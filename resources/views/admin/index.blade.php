<!DOCTYPE html>
<html lang="en">
<head>
  <title>Bootstrap Example</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/css/bootstrap.min.css">
  <script src="https://cdn.jsdelivr.net/npm/jquery@3.6.0/dist/jquery.slim.min.js"></script>
  <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"></script>
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/js/bootstrap.bundle.min.js"></script>

  <style>
    form,table{
        margin-top: 5%;
    }

  </style>
</head>
<body>

    <div class="container">

        <form id="formFile">
            @csrf
            <div class="row">
                <div class=col-md-10>
                    <input type="text" placeholder="path/file" id="file_path_id" name="file_path" class="form-control">
                </div>
                <div class=col-md-2>
                    <input value="View" class="btn btn-primary submit">
                </div>

            </div>
        <form>
        <div class="page" align="center">
            <table class="table" id="data" align="center">
                <thead>
                <tr>
                    <th>num</th>
                    <th>line</th>
                </tr>
                </thead>
                <tbody id="table_body">

                </tbody>
            </table>
        </div>
    </div>

</body>

<script src= "https://cdnjs.cloudflare.com/ajax/libs/axios/0.18.0/axios.js"></script>
<script src="https://code.jquery.com/jquery-3.5.1.min.js"> </script>
<script src="{{asset('assets/app.js')}}"> </script>

</html>
